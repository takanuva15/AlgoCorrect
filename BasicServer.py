import win32com.server.register
import win32com.server.register

from lib.py import pyuac


class BasicServer:
    _public_methods_ = ["ping", "saveStr"]
    _public_attrs_ = []
    _readonly_attrs_ = ["count"]
    _reg_clsid_ = "{D0D1E5D2-1266-458D-B2D6-A128C0CF70E3}"
    _reg_progid_ = "Python.BasicServer"
    _reg_desc_ = "Test COM server"

    count = 0

    def __init__(self):
        # self.count = 0
        self.save = ""

    def ping(self):
        # self.count = self.count + 1
        BasicServer.count += 1
        return BasicServer.count
        # return self.save

    def saveStr(self, saved_str):
        self.save = saved_str

    @staticmethod
    def reg():
        win32com.server.register.UseCommandLine(BasicServer)

    @staticmethod
    def unreg():
        if not pyuac.isUserAdmin():
            pyuac.runAsAdmin()
        win32com.server.register.UnregisterServer(BasicServer._reg_clsid_, BasicServer._reg_progid_)


if __name__ == "__main__":

    import sys

    if len(sys.argv) < 2:
        print("Error: need to supply arg (""--register"" or ""--unregister"")")
        sys.exit(1)
    elif sys.argv[1] == "--register":
        BasicServer.reg()
    elif sys.argv[1] == "--unregister":
        print("Starting to unregister...")
        BasicServer.unreg()
        print("Unregistered COM server.")
    else:
        print("Error: arg not recognized")
