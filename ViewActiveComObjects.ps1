Param($Filter = 'Python.*')

function Get-ComObject($Filter) {

	Write-Output "Filtering by: $Filter"
	#$ListofObjects = Get-ChildItem HKLM:\Software\Classes -ErrorAction SilentlyContinue | Where-Object {
	#		$_.PSChildName -match '^\w+\.\w+$' -and (Test-Path -Path "$( $_.PSPath )\CLSID")
	#	} | Select-Object -ExpandProperty PSChildName
	$ListofObjects = Get-ChildItem HKLM:\Software\Classes | Where-Object {
		$_.PSChildName -match $Filter
	} | Select-Object

	#	if($Filter) {
	#		$ListofObjects | Where-Object { $_ -like $Filter }
	#}
	#else {
	#	$ListofObjects
	#}
	Write-Output $ListofObjects
}

Get-ComObject -Filter $Filter
